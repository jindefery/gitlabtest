package main

import (
	"io"
	"log"
	"net/http"
)

func main() {
	// Hello world, the web server

	helloHandler := func(w http.ResponseWriter, req *http.Request) {
		io.WriteString(w, "Microservice-1 is Healthly.\n")
	}

	http.HandleFunc("/health", helloHandler)
    log.Println("Listing for requests at http://<IP Address>:8000/health")
	log.Fatal(http.ListenAndServe(":8000", nil))
}